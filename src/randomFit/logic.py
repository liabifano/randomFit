import os
import operator
import numpy as np

import xml.etree.ElementTree as ET


DATA_PATH = os.path.join(os.path.dirname(
    os.path.abspath(__file__)), 'data.xml')


def format_seconds(seconds):
    minutes = seconds // 60
    rest = seconds % 60

    if minutes == 0:
        return str(seconds) + 's'

    else:
        if rest != 0:
            return str(minutes) + 'm' + str(rest) + 's'
        else:
            return str(minutes) + 'm'


def get_time_slots(minutes, number, seed=None):
    seconds = minutes*60
    slots = np.arange(30, seconds, 30)

    if number == 1:
        intervals = [seconds]
    elif number == 2*minutes:
        intervals = [30]*number
    elif len(slots) <= number and number > 2:
        raise ValueError('Slow Down!!')
    else:
        np.random.seed(seed)
        splits = sorted([x + 1 for x in
                         np.random.choice(len(slots) - 1, number - 1, replace=False)])
        intervals = []
        for i in range(0, number):
            if i == 0:
                intervals.append(slots[splits[i] - 1])
            elif i == number - 1:
                intervals.append(slots[-1] - slots[splits[i - 1]] + 60)
            else:
                intervals.append(slots[splits[i]] - slots[splits[i-1]])

    return sorted(intervals)


def get_exs(number,
            category,
            data_path=DATA_PATH,
            seed=None):

    tree = ET.parse(data_path)
    all_exs = tree.getroot()

    if category == 'all':
        possible_exs = all_exs
    else:
        possible_exs = [e for e in all_exs
                        if any([x in category
                                for x in e.attrib['category'].split()])]

    if number > len(possible_exs):
        raise ValueError('There is no enough exercises available')

    np.random.seed(seed)
    selected_indexes = np.random.choice(
        len(possible_exs), number, replace=False)

    final_exs = operator.itemgetter(*list(selected_indexes))(possible_exs)
    final_exs = list(final_exs) if number > 1 else [final_exs]

    return sorted(final_exs, key=lambda x: int(x.find('level').text), reverse=True)


def merge_time_exs(slots, selected_exs, seed=None):

    join = list(zip(slots, selected_exs))
    np.random.seed(seed)
    np.random.shuffle(join)

    join = [{'name': x[1].find('name').text,
             'description': x[1].find('description').text,
             'time': format_seconds(x[0])} for x in join]

    return join


def get_result(minutes, number, ex_type):
    exs = get_exs(number, ex_type)
    slots = get_time_slots(minutes, number)

    return merge_time_exs(slots, exs)
