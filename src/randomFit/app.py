from flask import Flask, render_template, request, redirect, url_for, session
from flask_frozen import Freezer

import logic


app = Flask(__name__)
app.config['FREEZER_DESTINATION'] = 'public'
freezer = Freezer(app)
app.secret_key = 'super secret key'


@app.cli.command()
def freeze():
    freezer.freeze()


@app.cli.command()
def serve():
    freezer.run(debug=True)


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        return render_template('home.html')

    else:
        minutes = int(request.form['minutes'])
        number = int(request.form['number'])
        ex_type = request.form['type']
        session['input'] = {'number': number,
                            'minutes': minutes,
                            'ex_type': ex_type}

        return redirect(url_for('program'))


@app.route('/program')
def program():
    print(session)
    session['exs'] = logic.get_result(session['input']['minutes'],
                                      session['input']['number'],
                                      session['input']['ex_type'])

    return render_template('program.html', exercises=session['exs'])



@app.route('/run')
def run():
    return render_template('run.html', exercises=session['exs'])



if __name__ == '__main__':
    app.run(debug=True)
