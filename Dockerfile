FROM python:3.6.6

RUN python -m pip install pip --upgrade

COPY setup.py /randomFit/
COPY requirements.txt /randomFit/
COPY src/ /randomFit/src/

RUN find . | grep -E "(__pycache__|\.pyc$)" | xargs rm -rf
RUN pip install -U -r randomFit/requirements.txt
RUN pip install randomFit/.

WORKDIR /randomFit

EXPOSE 5000
